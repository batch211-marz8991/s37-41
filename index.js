const express = require("express");
const mongoose = require("mongoose")
//cors - Allows our backend aplication available in frontend application
const cors = require("cors");
const app = express();

mongoose.connect("mongodb+srv://admin123:admin123@project0.zprhgcb.mongodb.net/s37-41?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open',()=>console.log("Now connected to MongoDB Atlas."))

//ALLOWS ALL RESOUCES TO ACCESS BACKEND APPLICATION
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.listen(process.env.PORT || 4000,()=>{
	console.log(`API is now online on port ${process.env.PORT||4000}`)
});